import 'react-native-gesture-handler';
import React from 'react';
import { StyleSheet, Text, View, SafeAreaView } from 'react-native';

//import pages
import Home from './src/Screens/Home/home'
import SplashScreen from './src/Components/SplashScreen'


//import navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function Root() {
    return (
        <>
            <NavigationContainer>
                <Stack.Navigator initialRouteName="SplashScreen">
                    <Stack.Screen
                        options={{ headerShown: false }}
                        name="Home"
                        component={Home}
                    />
                    <Stack.Screen
                        options={{ headerShown: false }}
                        name="SplashScreen"
                        component={SplashScreen}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </>
    );
}




