import React from 'react'
import { View, Text } from 'react-native'

//import pages
import Home from './src/Screens/Home/home'
import Root from './Root'

//import redux
import { Store, Persistor } from './src/Store/Store'
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';


export default function App() {
  return (
    <Provider store={Store}>
      <PersistGate persistor={Persistor}>
        <Root />
      </PersistGate>
    </Provider>

  )
}
