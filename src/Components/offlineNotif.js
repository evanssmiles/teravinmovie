import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { moderateScale } from 'react-native-size-matters';
import {
    heightPercentageToDP,
    widthPercentageToDP,
} from 'react-native-responsive-screen';


const OfflineNotif = ({ connect }) => {

    return (
        <>
            {connect ? (
                <View style={styles.topNotifConnect}>
                    <Text style={styles.textNotif}>You Are Connected</Text>
                </View>
            ) : (
                <View style={styles.topNotif}>
                    <Text style={styles.textNotif}>No Internet Connection</Text>
                </View>
            )}
        </>
    );
};

export default OfflineNotif;

const styles = StyleSheet.create({
    topNotif: {
        backgroundColor: "red",
        height: heightPercentageToDP(2),
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        alignSelf: 'center',
        zIndex: 1,
    },
    topNotifConnect: {
        backgroundColor: "green",
        height: heightPercentageToDP(2),
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        alignSelf: 'center',
        zIndex: 1,
    },
});
