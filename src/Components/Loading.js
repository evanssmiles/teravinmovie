import React from 'react'
import { View, Text, ActivityIndicator } from 'react-native'

export default function Loading(props) {
    return (
        <View style={props.wrapper}>
            <ActivityIndicator color="#FF8749" size={50} />
            <Text style={{ alignSelf: "center" }}>Data sedang diperbaharui</Text>
        </View>
    )
}
