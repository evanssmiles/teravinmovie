import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { useNetInfo } from '@react-native-community/netinfo';
import OfflineNotif from './offlineNotif'
import { moderateScale } from 'react-native-size-matters';
import {
    heightPercentageToDP,
    widthPercentageToDP,
} from 'react-native-responsive-screen';

const Splash = ({ navigation }) => {
    const netInfo = useNetInfo();

    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Home');
        }, 3000);
    }, [navigation]);

    return (
        <View style={styles.container}>
            {!netInfo.isConnected ? <OfflineNotif /> : null}
            <Image source={require('../Assets/Images/logo_teravin.png')} style={{ height: moderateScale(40), width: moderateScale(150) }} />
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E9E8E5',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
