import axios from 'axios';
import { takeLatest, put } from 'redux-saga/effects';

//import action
import { setMovies } from './action';
import { GET_MOVIES } from "./action";
import { actionLoading } from '../../../Store/GlobalAction'

function* sagaGetMovies(action) {
    try {
        yield put(actionLoading(true))
        const result = yield axios.get(
            'https://api.themoviedb.org/3/discover/movie?api_key=f7b67d9afdb3c971d4419fa4cb667fbf',
            action.payload,
        );
        console.log(result, "hasil result");
        if (result.status === 200) {
            yield put(setMovies(result.data.results));
        }
        console.log(result.data.results, "ini set movies");
    } catch (error) {
        console.log(error.Error);
    } finally {
        yield put(actionLoading(false))
    }
}

function* sagaMovie() {
    yield takeLatest(GET_MOVIES, sagaGetMovies);
}

export default sagaMovie;
