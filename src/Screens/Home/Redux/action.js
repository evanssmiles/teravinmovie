export const SET_MOVIES = 'SET_MOVIES'
export const GET_MOVIES = 'GET_MOVIES'



export const setMovies = payload => {
    return {
        type: SET_MOVIES,
        payload,
    };
};

export const getMovies = payload => {

    return {
        type: GET_MOVIES,
        payload,
    }
}

