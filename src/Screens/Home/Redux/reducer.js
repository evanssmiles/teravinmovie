import { SET_MOVIES } from "./action";

const initialState = {
    data: [],

};

const MovieReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_MOVIES:
            return {
                ...state,
                data: action.payload,
            };

        default:
            return state;
    }
};

export default MovieReducer;
