import React, { useEffect, useState } from 'react'
import { View, SafeAreaView, Text, ScrollView } from 'react-native'
import FastImage from 'react-native-fast-image';
import { Button, Snackbar } from 'react-native-paper';

//import style
import styles from './styles'

//import components
import Loading from '../../Components/Loading'

//import redux action
import { useDispatch, useSelector } from 'react-redux';
import { getMovies } from './Redux/action'
import { moderateScale } from 'react-native-size-matters';


export default function home() {

    //statements
    const dispatch = useDispatch();

    //statements for snackbar
    const [visible, setVisible] = React.useState(false)
    const [loading, setLoading] = React.useState(false)
    const onToggleSnackBar = () => setVisible(true)
    const onDismissSnackBar = () => setVisible(false)


    //take data from reducer
    const dataMovies = useSelector(state => state.MovieReducer?.data);
    const isLoading = useSelector(state => state.Global.loading)
    console.log(dataMovies, "ini data movies lu di home")

    //use effects
    useEffect(() => {
        dispatch(getMovies())
        const interval = setInterval(() => {
            onToggleSnackBar()
        }, 60000)
        return () => clearInterval(interval)
    }, []);

    const showUpdateData = () => {
        dispatch(getMovies())
    }


    return (
        <View style={styles.container}>
            <View style={styles.headerContainer}>
                {/* ini view header */}
                <FastImage
                    style={styles.logoHeader}
                    source={require('../../Assets/Images/logo_teravin.png')}
                >

                </FastImage>
            </View>
            <ScrollView>
                {/* container movies */}
                {isLoading || loading ? (
                    <View style={{ marginTop: moderateScale(270) }}>

                        <Loading />
                    </View>

                ) : (<>
                    {dataMovies.sort(function (a, b) { return b.popularity - a.popularity }).slice(0, 10).map((e, i) => {
                        return (
                            <View key={i} style={styles.listData}>
                                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                    <Text style={{ fontSize: moderateScale(20) }}>{e.original_title}</Text>
                                    <FastImage
                                        source={{ uri: `https://image.tmdb.org/t/p/original${e.poster_path}` }}
                                        resizeMode="contain"
                                        style={{ width: moderateScale(80), height: moderateScale(150) }}
                                    />
                                </View>
                                <Text>Release Date : {e.release_date}</Text>
                            </View>
                        )

                    })}





                </>)}

            </ScrollView>
            <Snackbar
                style={{ zIndex: 100 }}
                visible={visible}
                duration={5000}
                onDismiss={onDismissSnackBar}
                action={{
                    label: 'Tampilkan',
                    onPress: () => {
                        showUpdateData()
                        setLoading(true)
                        setTimeout(() => {
                            setLoading(false)
                        }, 1500);
                    },
                }}>
                Penyimpanan lokal telah diperbaharui.
                        </Snackbar>
        </View>
    )
}
