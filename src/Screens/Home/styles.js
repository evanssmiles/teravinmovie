import { StyleSheet, Dimensions } from 'react-native';
import { moderateScale } from 'react-native-size-matters';
import {
    heightPercentageToDP,
    widthPercentageToDP,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },

    headerContainer: {
        height: moderateScale(60),
        width: widthPercentageToDP(100),
        backgroundColor: "#E9E8E5",
        paddingTop: moderateScale(7)

    },

    logoHeader: {
        height: moderateScale(40),
        width: moderateScale(150)
    },

    listData: {
        backgroundColor: 'white',
        elevation: 10,
        marginBottom: moderateScale(10),
        padding: moderateScale(10),
        textAlign: 'justify',
        height: moderateScale(200),
    },

    containerSnackBar: {
        flex: 1,
        justifyContent: 'space-between',
    }
});

export default styles;
