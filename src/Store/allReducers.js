import { combineReducers } from 'redux';

//reducers
import GlobalReducer from './GlobalReducer';
import MovieReducer from '../Screens/Home/Redux/reducer'

export const allReducers = combineReducers({
    Global: GlobalReducer,
    MovieReducer,
});
