import { all } from 'redux-saga/effects'

//screen saga
// import sagaRegister from '../Screen/Auth/Register/Redux/sagaRegister';
import sagaMovie from '../Screens/Home/Redux/sagaMovie'



function* SagaWatcher() {
    yield all([
        sagaMovie(),
    ])
}

export default SagaWatcher;