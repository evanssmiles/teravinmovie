import { createStore, applyMiddleware } from 'redux';
import { allReducers } from './allReducers';
import { persistReducer, persistStore } from 'redux-persist';
import Storage from '@react-native-async-storage/async-storage';

//import redux
import logger from 'redux-logger';
import createSaga from 'redux-saga'
import SagaWatcher from './SagaWatcher';


const persistConfig = {
    key: 'teravinDB',
    storage: Storage,
};

const Saga = createSaga();
const persistedReducer = persistReducer(persistConfig, allReducers);

export const Store = createStore(
    persistedReducer,
    applyMiddleware(Saga, logger));

export const Persistor = persistStore(Store);
Saga.run(SagaWatcher);